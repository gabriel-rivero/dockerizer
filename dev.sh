#!/bin/bash

if [ "$#" -eq 0 ]; then
  echo "
  Please provide a tag for building your docker images and starting your containers
    - ./dev.sh {TAG}
  "
else
  COMMAND=$2
  if [ -z "$COMMAND" ]
  then
    COMMAND="dev"
  fi

    TAG=$1 \
    COMMAND=$COMMAND \
    docker-compose \
      --file docker-compose.development.yaml \
      up \
        --build
fi
