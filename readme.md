# for development

```./dev.sh <tag>```

# for deploy

## Create cluster

1. create eks role https://console.aws.amazon.com/iam/home?#/roles
2. create cluster https://console.aws.amazon.com/eks/home?region=us-east-1#/home

## install kubernetes

1. https://kubernetes.io/docs/tasks/tools/install-kubectl/

## install docker

1. https://docs.docker.com/get-docker/

## install helm

1. https://helm.sh/docs/intro/install/

# install IAM auth

1. https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html

# make sure there's an id on AWS

1. https://docs.aws.amazon.com/IAM/latest/UserGuide/getting-started_create-admin-group.html
2. write down AWS Access Key ID & AWS Secret Access Key

## install AWS cli (to deploy to EKS)

1. https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html
2. configure https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html <- use here the credentials from before

## configure kubectl

1. ```aws eks --region <region> update-kubeconfig --name <clusterName>```
for me it was:

```aws-iam-authenticator token -i demo-cluster```
```export AWS_SECRET_ACCESS_KEY="82FqHFzPCEiTSgvzlJj4R+fNdt33ldoPUEQpYfZQ"```
```export AWS_ACCESS_KEY_ID="AKIA2RLASHHK4AYGGCWK"```
```export AWS_SESSION_TOKEN="k8s-aws-v1.aHR0cHM6Ly9zdHMuYW1hem9uYXdzLmNvbS8_QWN0aW9uPUdldENhbGxlcklkZW50aXR5JlZlcnNpb249MjAxMS0wNi0xNSZYLUFtei1BbGdvcml0aG09QVdTNC1ITUFDLVNIQTI1NiZYLUFtei1DcmVkZW50aWFsPUFLSUEyUkxBU0hISzRBWUdHQ1dLJTJGMjAyMDAzMjElMkZ1cy1lYXN0LTElMkZzdHMlMkZhd3M0X3JlcXVlc3QmWC1BbXotRGF0ZT0yMDIwMDMyMVQxODUzMTFaJlgtQW16LUV4cGlyZXM9MCZYLUFtei1TZWN1cml0eS1Ub2tlbj1rOHMtYXdzLXYxLmFIUjBjSE02THk5emRITXVZVzFoZW05dVlYZHpMbU52YlM4X1FXTjBhVzl1UFVkbGRFTmhiR3hsY2tsa1pXNTBhWFI1SmxabGNuTnBiMjQ5TWpBeE1TMHdOaTB4TlNaWUxVRnRlaTFCYkdkdmNtbDBhRzA5UVZkVE5DMUlUVUZETFZOSVFUSTFOaVpZTFVGdGVpMURjbVZrWlc1MGFXRnNQVUZMU1VFeVVreEJVMGhJU3pSQldVZEhRMWRMSlRKR01qQXlNREF6TWpFbE1rWjFjeTFsWVhOMExURWxNa1p6ZEhNbE1rWmhkM00wWDNKbGNYVmxjM1FtV0MxQmJYb3RSR0YwWlQweU1ESXdNRE15TVZReE9EUTBNemxhSmxndFFXMTZMVVY0Y0dseVpYTTlNQ1pZTFVGdGVpMVRaV04xY21sMGVTMVViMnRsYmoxck9ITXRZWGR6TFhZeExtRklVakJqU0UwMlRIazVlbVJJVFhWWlZ6Rm9aVzA1ZFZsWVpIcE1iVTUyWWxNNFgxRlhUakJoVnpsMVVGVmtiR1JGVG1oaVIzaHNZMnRzYTFwWE5UQmhXRkkxU214YWJHTnVUbkJpTWpRNVRXcEJlRTFUTUhkT2FUQjRUbE5hV1V4VlJuUmxhVEZDWWtka2RtTnRiREJoUnpBNVVWWmtWRTVETVVsVVZVWkVURlpPU1ZGVVNURk9hVnBaVEZWR2RHVnBNVVJqYlZacldsYzFNR0ZYUm5OUVZVWk1VMVZGZVZWcmVFSlZNR2hKVTNwU1FsZFZaRWhSTVdSTVNsUktSMDFxUVhsTlJFRjZUV3BGYkUxcldqRmplVEZzV1ZoT01FeFVSV3hOYTFwNlpFaE5iRTFyV21oa00wMHdXRE5LYkdOWVZteGpNMUZ0VjBNeFFtSlliM1JTUjBZd1dsUXdlVTFFU1hkTlJFMTVUVlpSZUU5RVVYZE5hbWhoU214bmRGRlhNVFpNVlZZMFkwZHNlVnBZVFRsTlExcFpURlZHZEdWcE1WUmhWMlIxV2xkU1NWcFhSbXRhV0VwNlVGZG9kbU16VVd4Tk1FbzBURmR6TkdONU1XaGtNMDEwWVZkUmJWZERNVUppV0c5MFZUSnNibUp0UmpCa1dFcHNVRmROTVUxNlVtdGFhbHBwVG1wcmVVMXFWbXhaVjBsNFdWUnJNMWxYVVRWUFZFcG9UVmRLYkZsVVozcFpWRVp0V2xkU2JVNTZWVE5QVjBreVRXcEJNVmx0Um1oYWJWcHFUVzFXYlUweVJYbE5WR2N6VFhwRkpsZ3RRVzE2TFZOcFoyNWxaRWhsWVdSbGNuTTlhRzl6ZENVelFuZ3Rhemh6TFdGM2N5MXBaQ1pZTFVGdGVpMVRhV2R1WVhSMWNtVTlNR0UyWXpWbE1UUmlOVEV4TjJZek5UZzNObU16WlRNMVkyUTVOMkpsTjJJelpUUTBOemd5T0daaE16QTFNakU0TkRCaVpEUmpOV1UxTUROaE9EZGlNQSZYLUFtei1TaWduZWRIZWFkZXJzPWhvc3QlM0J4LWs4cy1hd3MtaWQmWC1BbXotU2lnbmF0dXJlPTEyNmU4NGY1ZjRlMTkzMjM4ZTY2NGFiODg1Y2IyZDY5NzVmNTNjNjNjNWM4YTRhZDdhYTUwNDcwNTIwZDIxNjk"```
```aws eks update-kubeconfig --name demo-cluster --region us-east-1 --role-arn arn:aws:iam::724441381333:role/cluster-role```
```kubectl get svc```

```export AWS_SECRET_ACCESS_KEY=""```
```export AWS_ACCESS_KEY_ID=""```
```export AWS_SESSION_TOKEN=""```

### create a stack of workers from cloudformation

1. https://console.aws.amazon.com/cloudformation/home
2. use template from ```https://s3-external-1.amazonaws.com/cf-templates-1ihuty459m5jc-us-east-1/2020081IoR-test-template5wc01aur8om```
3. give unique values to names
4. check the ami id from this link https://docs.aws.amazon.com/eks/latest/userguide/eks-optimized-ami.html (I used ```ami-0fc61db8544a617ed```
)
5. make sure to select the key pair
6. make sure to select VPC and subnet
7. make sure to check the last box on the capabilities section from the last screen
8. type down the node instance role (stack-NodeInstanceRole-ZOV3W7590E3Q)
9. create a local file for kubernetes to know your stack
```vim ~/.kube/aws-auth-cm.yaml```
```
apiVersion: v1
kind: ConfigMap
metadata:
 name: aws-auth
 namespace: kube-system
data:
 mapRoles: |
   - rolearn: stack-NodeInstanceRole-ZOV3W7590E3Q
     username: system:node:{{EC2PrivateDNSName}}
     groups:
       - system:bootstrappers
       - system:nodes
```
10. apply the changes to kubernetes ```kubectl apply -f ~/.kube/aws-auth-cm.yaml```

## Prepare images to be uploaded to docker hub

1. ```docker login``` use credentials from docker hub

## build and push publish images

1. ```./publish.sh someTag hybrael```
2. at this point the images should be visible at docker hub https://hub.docker.com/

## use helm to package the deploy

1. edit dockerizer-chart/values.yml to reflect the current tag (this deploy is "manual")
2.a. install the chart ```helm install dockerizer ./dockerizer-chart```
2.b. upgrade ```helm upgrade dockerizer ./dockerizer-chart```
