#!/bin/bash

if [ $# -lt 2 ]; then
  echo "A tag and docker username is required ex: ./publish.sh tag1 demouser"
  exit 1
fi

USER=$2

TAG=$1 \
USER=$USER \
docker-compose \
  --file docker-compose.publish.yaml \
  build

# add here the images that need to be pushed
docker push $USER/python:build-$TAG
docker push $USER/angular:build-$TAG

# kubectl apply -f python.yaml
# kubectl apply -f python-service.yaml
